import React from "react";

function useValidationFunction() {
  // Check if a value is empty
  const isEmpty = (value) => {
    if (value === null || value === undefined) {
      return true;
    }
    if (typeof value === "string" && value.trim() === "") {
      return true;
    }
    if (Array.isArray(value) && value.length === 0) {
      return true;
    }
    if (typeof value === "object" && Object.keys(value).length === 0) {
      return true;
    }
    return false;
  };

  // Validate email format
  const isValidEmail = (email) => {
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(String(email).toLowerCase());
  };

  // Get the first error message from an error object
  const getFirstErrorMessage = (errors) => {
    for (const key in errors) {
      if (errors.hasOwnProperty(key)) {
        return errors[key];
      }
    }
    return "No errors found";
  };

  // Validate form data
  const validateData = (state) => {
    const newErrors = {};
    for (const key in state) {
      if (state.hasOwnProperty(key)) {
        if (isEmpty(state[key])) {
          newErrors[key] = `${key} field is required`;
        } else if (key.toLowerCase().includes("email") && !isValidEmail(state[key])) {
          newErrors[key] = `Invalid email format for ${key}`;
        }
      }
    }
    return newErrors;
  };

  // Get password strength message
  const getPasswordStrengthMessage = (password) => {
    if (!password || password.length === 0) {
      return "Password is required";
    } else if (password.length < 8) {
      return "Password must be at least 8 characters long";
    } else {
      const strongPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,}$/;
      const mediumPattern = /^(?=.*\d)(?=.*[a-zA-Z]).{8,}$/;
      if (strongPattern.test(password)) {
        return "Strong password";
      } else if (mediumPattern.test(password)) {
        return "Medium password";
      } else {
        return "Weak password";
      }
    }
  };

  return {
    validateData,
    getFirstErrorMessage,
    isEmpty,
    getPasswordStrengthMessage,
  };
}

export default useValidationFunction;
