import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectIsLogin, setIsLogin } from "../redux/LoginReducer";
import { getLocalStorage, removeLocalStorage, setLocalStorage } from "../components/common/localStorage";
import { ROUTES } from "../reactRoute/RouteConstants";
import { useLocation, useNavigate } from "react-router-dom";
import { Config } from "../constants/Index";

function useHandleLogin() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const isLogin = useSelector(selectIsLogin) || getLocalStorage(Config.token);
  console.log(isLogin, "isLogin")
  const handleLogin = () => {
    dispatch(setIsLogin(true));
    setLocalStorage(Config.token, Config.userToken);
    navigate(ROUTES.content);
  };

  const handleLogout = () => {
    console.log("LogOut")
    removeLocalStorage(Config.token);
    dispatch(setIsLogin(false));
    navigate(ROUTES.login);
  };
  return { isLogin, handleLogin, handleLogout };
}

export default useHandleLogin;
