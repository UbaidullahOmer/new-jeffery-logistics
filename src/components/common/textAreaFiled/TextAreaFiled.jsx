import React from "react";

function TextAreaFiled({
  value,
  onChange,
  placeholder = "Enter here",
  className,
  onBlur,
  onKeyDown,
  name,
  type = "text",
  error = "",
}) {
  return (
    <span>
      <span className="text-red-500 text-[12px]">{error}</span>
      <textarea
        value={value}
        type={type}
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        className={`${className} w-full h-full  ${
          error ? "border-red-500 border-2 rounded" : "outline-none"
        }`}
        onBlur={onBlur}
        onKeyDown={onKeyDown}
      />
    </span>
  );
}

export default TextAreaFiled;
