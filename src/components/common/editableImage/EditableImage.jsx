import React from "react";

function EditableImage({ onChange, src, className, style, onClick, alt, hight, width, divClassName }) {
  return (
    <div className={`w-fit h-fit relative group cursor-pointer ${divClassName}`}>
      <i class="ri-edit-2-fill absolute top-[50%] group-hover:opacity-100 group-hover:scale-100 rotate-0 group-hover:rotate-[360deg] transition left-[50%] opacity-0 scale-0 -translate-x-[50%] -translate-y-[50%] w-[56px] h-[56px] text-[28px] bg-[#e7e6e6] flex items-center justify-center rounded-full z-[1] shadow cursor-pointer text-[#FF5C35]"></i>
      <input onChange={onChange} type="file"  className="opacity-0 absolute inset-0 z-[1]" />
      <img
        src={src}
        className={className}
        style={style}
        onClick={onClick}
        hight={hight}
        width={width}
        alt={alt}
      />
    </div>
  );
}

export default EditableImage;
