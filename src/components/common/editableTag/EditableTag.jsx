import React, { useState } from "react";
import { getLocalStorage } from "../localStorage";
import useHandleLanguage from "../../../hooks/useHandleLanguage";

function EditableTag({
  children,
  className,
  style,
  id,
  onClick,
  onKeyDown,
  onInput,
  onBlur,
  tag,
}) {
  const Element = tag || "p";
  const {selectedLanguage} = useHandleLanguage()
  let text = children;
  if (typeof children === "object" && children !== null) {
    text = children[selectedLanguage || "en"] || children["en"];
  }

  return (
    <Element
      contentEditable
      className={className}
      style={style}
      id={id}
      onClick={onClick}
      onKeyDown={onKeyDown}
      onInput={onInput}
      onBlur={onBlur}
      suppressContentEditableWarning={true}
    >
      {text}
    </Element>
  );
}

export default EditableTag;
