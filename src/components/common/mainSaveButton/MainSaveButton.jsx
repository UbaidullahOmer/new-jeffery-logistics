import React from "react";

function MainSaveButton({ onClick, className, onResetData, title = "" }) {
  return (
    <span className={mainDivStyle}>
      <h1 className={buttonTitle}>{title}</h1>
      <span onClick={onClick} className={mainSaveButtonStyle}>
        Save
      </span>
      <span onClick={onResetData} className={mainResetButtonStyle}>
        Reset
      </span>
    </span>
  );
}
const buttonTitle =
  "text-[24px]  font-[500] cursor-pointer hover:scale-110 transition";
const mainDivStyle = `fixed right-[50%] z-[10] -translate-x-[-50%] top-[40px] flex gap-[16px]  items-center bg-[#FFFFFF] px-[32px] py-[14px] rounded-[10px]`;
const mainSaveButtonStyle = ` font-[500] px-[32px] py-[14px]  cursor-pointer hover:scale-110 transition rounded-[10px] bg-[#FF5C35] text-[#FFFFFF]`;
const mainResetButtonStyle = `font-[500] px-[32px] py-[14px]  cursor-pointer hover:scale-110 transition rounded-[10px] bg-[#FFFFFF] text-[#FF5C35]`;

export default MainSaveButton;
