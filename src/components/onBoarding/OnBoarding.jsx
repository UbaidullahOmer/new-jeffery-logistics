import React from 'react'

function OnBoarding() {
  return (
    <div className="sec4">
      <div className="sec4-2">
        <div className="sec4-1">
          <div className="sec4-1-prt1">
            <div className="sec4-1-prt1-txt">ONBOARDING</div>
            <div className="sec4-1-prt1-line"></div>
          </div>
          <div className="sec4-1-prt2">Get Onboard as a Carrier</div>
          <div className="sec4-1-prt3">
            Easy four steps for a driver to get onboard with us.
          </div>
        </div>
        <div className="sec4-3">
          <div className="sec4-3-prt1">
            <div className="sec4-3-prt1-icn">
              <svg
                className="sec4-3-prt1-icn1"
                width="32"
                height="32"
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M27.3337 9.33341L16.0003 2.66675L4.66699 9.33341V22.6667L16.0003 29.3334L27.3337 22.6667V9.33341Z"
                  stroke-width="2.5"
                  stroke-linejoin="round"
                />
                <path
                  d="M10.667 12.6655L15.9957 16.0002L21.3303 12.6655M16.0003 16.0002V22.0002"
                  stroke-width="2.5"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </div>

            <div className="sec4-3-prt1-txt" style={{ color: '#091242' }}>Complete the Online Application</div>
            <div className="sec4-3-prt1-arrow1 max-sm:!mt-[4px]">
              <svg
                className="sec4-3-prt1-arrow-wdth"
                width="101"
                height="16"
                viewBox="0 0 101 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M100.707 8.70711C101.098 8.31658 101.098 7.68342 100.707 7.29289L94.3431 0.928932C93.9526 0.538408 93.3195 0.538408 92.9289 0.928932C92.5384 1.31946 92.5384 1.95262 92.9289 2.34315L98.5858 8L92.9289 13.6569C92.5384 14.0474 92.5384 14.6805 92.9289 15.0711C93.3195 15.4616 93.9526 15.4616 94.3431 15.0711L100.707 8.70711ZM0 9H3.125V7H0V9ZM9.375 9H15.625V7H9.375V9ZM21.875 9H28.125V7H21.875V9ZM34.375 9H40.625V7H34.375V9ZM46.875 9H53.125V7H46.875V9ZM59.375 9H65.625V7H59.375V9ZM71.875 9H78.125V7H71.875V9ZM84.375 9H90.625V7H84.375V9ZM96.875 9H100V7H96.875V9Z"
                  fill="#000024"
                  fill-opacity="0.16"
                />
              </svg>
            </div>
          </div>

          <div className="sec4-3-prt2">
            <div className="sec4-3-prt1-icn">
              <svg
                className="sec4-3-prt1-icn1"
                width="32"
                height="32"
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g clip-path="url(#clip0_40_695)">
                  <circle
                    cx="16"
                    cy="15.9998"
                    r="12"
                    stroke-width="2.5"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <path
                    d="M20 13.3333L14.6667 18.6666L12 15.9999"
                    stroke-width="2.5"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_40_695">
                    <rect width="32" height="32" fill="white" />
                  </clipPath>
                </defs>
              </svg>
            </div>
            <div className="sec4-3-prt1-txt" style={{ color: '#091242' }}>
              Verification and Compliance Check
            </div>
            <div className="sec4-3-prt1-arrow2 max-sm:!mt-[4px]">
              <svg
                className="sec4-3-prt1-arrow-wdth"
                width="101"
                height="16"
                viewBox="0 0 101 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M100.707 8.70711C101.098 8.31658 101.098 7.68342 100.707 7.29289L94.3431 0.928932C93.9526 0.538408 93.3195 0.538408 92.9289 0.928932C92.5384 1.31946 92.5384 1.95262 92.9289 2.34315L98.5858 8L92.9289 13.6569C92.5384 14.0474 92.5384 14.6805 92.9289 15.0711C93.3195 15.4616 93.9526 15.4616 94.3431 15.0711L100.707 8.70711ZM0 9H3.125V7H0V9ZM9.375 9H15.625V7H9.375V9ZM21.875 9H28.125V7H21.875V9ZM34.375 9H40.625V7H34.375V9ZM46.875 9H53.125V7H46.875V9ZM59.375 9H65.625V7H59.375V9ZM71.875 9H78.125V7H71.875V9ZM84.375 9H90.625V7H84.375V9ZM96.875 9H100V7H96.875V9Z"
                  fill="#000024"
                  fill-opacity="0.16"
                />
              </svg>
            </div>
          </div>

          <div className="sec4-3-prt3">
            <div className="sec4-3-prt1-icn">
              <svg
                className="sec4-3-prt1-icn1"
                width="32"
                height="32"
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g clip-path="url(#clip0_41_1290)">
                  <path
                    d="M5.33301 24.0001C5.33301 21.0546 7.72082 18.6667 10.6663 18.6667H21.333C24.2785 18.6667 26.6663 21.0546 26.6663 24.0001V24.0001C26.6663 25.4728 25.4724 26.6667 23.9997 26.6667H7.99968C6.52692 26.6667 5.33301 25.4728 5.33301 24.0001V24.0001Z"
                    stroke-width="2.5"
                    stroke-linejoin="round"
                  />
                  <circle cx="16" cy="9.33325" r="4" stroke-width="2.5" />
                </g>
                <defs>
                  <clipPath id="clip0_41_1290">
                    <rect width="32" height="32" fill="white" />
                  </clipPath>
                </defs>
              </svg>
            </div>
            <div className="sec4-3-prt1-txt" style={{ color: '#091242' }}>Onboarding</div>
          </div>

          <div className="sec4-3-prt4">
            <div className='sec4-3-prt1-icn'>
              <svg
                className="sec4-3-prt1-icn2"
                width="30"
                height="28"
                viewBox="0 0 30 28"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M2 2V0.75C1.30964 0.75 0.75 1.30964 0.75 2L2 2ZM16.6667 2H17.9167C17.9167 1.30964 17.357 0.75 16.6667 0.75V2ZM16.6667 10V8.75C16.3351 8.75 16.0172 8.8817 15.7828 9.11612C15.5484 9.35054 15.4167 9.66848 15.4167 10L16.6667 10ZM2 3.25H16.6667V0.75H2V3.25ZM15.4167 2V23.3333H17.9167V2H15.4167ZM3.25 20.6667V2H0.75V20.6667H3.25ZM16.6667 11.25H23.3333V8.75H16.6667V11.25ZM27.4167 15.3333V20.6667H29.9167V15.3333H27.4167ZM17.9167 23.3333L17.9167 10L15.4167 10L15.4167 23.3333L17.9167 23.3333ZM24.3351 24.3351C23.7818 24.8883 22.8848 24.8883 22.3316 24.3351L20.5638 26.1028C22.0934 27.6324 24.5733 27.6324 26.1028 26.1028L24.3351 24.3351ZM22.3316 22.3316C22.8848 21.7784 23.7818 21.7784 24.3351 22.3316L26.1028 20.5638C24.5733 19.0343 22.0934 19.0343 20.5638 20.5638L22.3316 22.3316ZM8.33507 24.3351C7.78182 24.8883 6.88484 24.8883 6.3316 24.3351L4.56383 26.1028C6.09339 27.6324 8.57328 27.6324 10.1028 26.1028L8.33507 24.3351ZM6.3316 22.3316C6.88484 21.7784 7.78182 21.7784 8.33507 22.3316L10.1028 20.5638C8.57328 19.0343 6.09339 19.0343 4.56383 20.5638L6.3316 22.3316ZM24.3351 22.3316C24.6119 22.6085 24.75 22.9688 24.75 23.3333H27.25C27.25 22.3329 26.8674 21.3284 26.1028 20.5638L24.3351 22.3316ZM24.75 23.3333C24.75 23.6978 24.6119 24.0582 24.3351 24.3351L26.1028 26.1028C26.8674 25.3383 27.25 24.3338 27.25 23.3333H24.75ZM20.6667 22.0833H16.6667V24.5833H20.6667V22.0833ZM22.3316 24.3351C22.0547 24.0582 21.9167 23.6978 21.9167 23.3333H19.4167C19.4167 24.3338 19.7993 25.3383 20.5638 26.1028L22.3316 24.3351ZM21.9167 23.3333C21.9167 22.9688 22.0547 22.6085 22.3316 22.3316L20.5638 20.5638C19.7993 21.3284 19.4167 22.3329 19.4167 23.3333H21.9167ZM6.3316 24.3351C6.05473 24.0582 5.91667 23.6978 5.91667 23.3333H3.41667C3.41667 24.3338 3.7993 25.3383 4.56383 26.1028L6.3316 24.3351ZM5.91667 23.3333C5.91667 22.9688 6.05473 22.6085 6.3316 22.3316L4.56383 20.5638C3.7993 21.3284 3.41667 22.3329 3.41667 23.3333H5.91667ZM16.6667 22.0833H10V24.5833H16.6667V22.0833ZM8.33507 22.3316C8.61193 22.6085 8.75 22.9688 8.75 23.3333H11.25C11.25 22.3329 10.8674 21.3284 10.1028 20.5638L8.33507 22.3316ZM8.75 23.3333C8.75 23.6978 8.61193 24.0582 8.33507 24.3351L10.1028 26.1028C10.8674 25.3383 11.25 24.3338 11.25 23.3333H8.75ZM27.4167 20.6667C27.4167 21.4491 26.7824 22.0833 26 22.0833V24.5833C28.1631 24.5833 29.9167 22.8298 29.9167 20.6667H27.4167ZM23.3333 11.25C25.5885 11.25 27.4167 13.0782 27.4167 15.3333H29.9167C29.9167 11.6975 26.9692 8.75 23.3333 8.75V11.25ZM0.75 20.6667C0.75 22.8298 2.50355 24.5833 4.66667 24.5833V22.0833C3.88426 22.0833 3.25 21.4491 3.25 20.6667H0.75Z"
                  fill="#FFB612"
                />
              </svg>
            </div>
            <div className="sec4-3-prt1-txt" style={{ color: '#091242' }}>Begin Receiving Loads</div>
            <div className="sec4-3-prt1-arrow4 max-sm:!mt-[4px]">
              <svg
                className="sec4-3-prt1-arrow-wdth"
                width="101"
                height="16"
                viewBox="0 0 101 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M100.707 8.70711C101.098 8.31658 101.098 7.68342 100.707 7.29289L94.3431 0.928932C93.9526 0.538408 93.3195 0.538408 92.9289 0.928932C92.5384 1.31946 92.5384 1.95262 92.9289 2.34315L98.5858 8L92.9289 13.6569C92.5384 14.0474 92.5384 14.6805 92.9289 15.0711C93.3195 15.4616 93.9526 15.4616 94.3431 15.0711L100.707 8.70711ZM0 9H3.125V7H0V9ZM9.375 9H15.625V7H9.375V9ZM21.875 9H28.125V7H21.875V9ZM34.375 9H40.625V7H34.375V9ZM46.875 9H53.125V7H46.875V9ZM59.375 9H65.625V7H59.375V9ZM71.875 9H78.125V7H71.875V9ZM84.375 9H90.625V7H84.375V9ZM96.875 9H100V7H96.875V9Z"
                  fill="#000024"
                  fill-opacity="0.16"
                />
              </svg>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default OnBoarding