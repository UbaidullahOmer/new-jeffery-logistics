import React from 'react'

function TrustedDispatch() {
  return (
<div className="sec3">
        <div className="sec3-1">
          <div className="sec3-1-prt1">
            <div className="sec3-1-prt1-1">
              <div className="sec3-1-prt1-txt1">TRUSTED DISPATCH SOLUTIONS</div>
              <div className="sec3-1-prt1-line"></div>
            </div>
            <div className="sec3-1-prt1-txt2">
              No Hidden Fees, No Additional Expenses
            </div>
            <div className="sec3-1-prt1-txt3">
              You are missing a significant portion of your load if you try to
              handle it on your own. Our team of professionals can help.
            </div>
          </div>
          <div className="sec3-1-prt2">
            <img className="sec3-1-prt2-img" src="images/sec3-img-sm.png" alt="" />
            <img className="sec3-1-prt2-img1" src="images/sec3-img-lg.png" alt="" />
          </div>
        </div>
      </div>
  )
}

export default TrustedDispatch